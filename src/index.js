firebase.auth().onAuthStateChanged((user) => {
    if (user) {
      // User is signed in, see docs for a list of available properties
      // https://firebase.google.com/docs/reference/js/firebase.User
      document.getElementById("josh_logged_in").style.display = "block";
      document.getElementById("josh_login").style.display = "none"
      
      
    } else {
      // User is signed out

      document.getElementById("josh_logged_in").style.display = "none";
      document.getElementById("josh_login").style.display = "block";
      
    }
  });

function storeData() {
    firebase.database().ref("user").set({
        name: document.getElementById("nameField").value,
        age: document.getElementById("ageField").value
    });
}

function login() {
    
    var joshEmail = document.getElementById("emailField").value;
    var joshPassword = document.getElementById("passwordField").value;

    firebase.auth().signInWithEmailAndPassword(joshEmail, joshPassword).catch((error) => {
        
      var errorCode = error.code;
        var errorMessage = error.message;

        window.alert("Error: " + errorMessage)

    });

}

function logout() {

  firebase.auth().signOut();

}